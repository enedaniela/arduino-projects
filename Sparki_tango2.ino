#include<sparki.h>

void setup()
{
  sparki.moveForward();
  delay(2000);
  sparki.moveRight();
  delay(1000);
  sparki.moveLeft();
  delay(1000);
  sparki.moveForward();
  delay(2000);
  sparki.moveLeft();
  delay(1000);
  sparki.moveRight();
  delay(1000);
  sparki.moveForward();
  delay(4000);
  
  sparki.moveRight(45);
  sparki.moveForward();
  delay(4000);
  sparki.servo(SERVO_LEFT);
  delay(1300);
  sparki.servo(SERVO_CENTER);
  sparki.moveLeft(360);
  sparki.servo(SERVO_LEFT);
  delay(1000);
  sparki.servo(SERVO_CENTER);
  sparki.moveLeft(90);
  sparki.moveForward();
  delay(1000);
  sparki.moveLeft(360);
  sparki.moveRight(45);
  sparki.servo(SERVO_LEFT);
  delay(1000);
  sparki.servo(SERVO_CENTER);
  
  // final
  sparki.moveBackward();
  delay(3000);
  sparki.moveLeft(30);
  sparki.moveForward();
  delay(4500);
  sparki.moveStop();
  sparki.gripperClose();
  delay(5500);
  sparki.RGB(RGB_RED);
  delay(1000);
  sparki.RGB(RGB_GREEN);
  delay(1000);
  sparki.RGB(RGB_RED);
  delay(1000);
  
  
  sparki.gripperOpen();
  delay(900);
  sparki.moveBackward();
  delay(2000);
  
  sparki.moveStop();
  
   
}

void loop()
{

}
